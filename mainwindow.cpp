#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDebug>

#define thumbnail_size 196

QString gallery_dir = QString("./gallery");
QString probe_dir = QString("./probe");
QString result_dir = QString("./results");
QString temp_dir = QString("./temp");
QString signature_dir = QDir(result_dir).filePath(QString("signature"));
QString score_dir = QDir(result_dir).filePath(QString("score"));
QString log_dir = QDir(result_dir).filePath(QString("log"));
QString log_file;

QString one_probe_list_file = QDir(temp_dir).filePath(QString("one_probe.txt"));
QString enrolled_gallery_list_file = QDir(result_dir).filePath(QString("gallery_list.txt"));
QString enrolled_probe_list_file = QDir(result_dir).filePath(QString("probe_list.txt"));
QString config_file = QString("./config.xml");

QString current_selected_id;
QString current_selected_path;

bool flag_2d = true;

QImage scale(const QString &image_path)
{
    QImage image(image_path);
    image = image.scaledToWidth(thumbnail_size, Qt::SmoothTransformation);
    return image;
}

QImage enroll(const QString &input_path)
{
    QString image_path = input_path;
    if(!flag_2d){
        QFileInfo fileInfo(input_path);
        QString ext = fileInfo.suffix();
        QStringList img2D_ext_list = (QStringList() << "bmp" << "jpg");
        for (int j = 0; j < img2D_ext_list.size(); j++){
            QString new_ext = img2D_ext_list.at(j);
            image_path = image_path.replace(ext, new_ext);
            if(QFileInfo(image_path).exists())
                break;
        }
    }
    QImage image(image_path);
    image = image.scaledToWidth(thumbnail_size, Qt::SmoothTransformation);

    QString output_path;
    QProcess p;
    if (flag_2d){
        output_path = QDir(signature_dir).filePath(QFileInfo(input_path).baseName() + ".mat");
        output_path = QFileInfo(output_path).absoluteFilePath();

        if (!QFileInfo(output_path).exists()){
            // call UR2D pipeline
            QStringList args = QStringList() << config_file << input_path << output_path;
            p.execute("./enroll", args);
        }
    }else{
        // call UR3D pipeline
        output_path = QDir(signature_dir).filePath(QFileInfo(input_path).baseName() + ".fwv");
        output_path = QFileInfo(output_path).absoluteFilePath();
        if (!QFileInfo(output_path).exists()){
            QStringList args = QStringList()<<"fun_UR3D_enroll.py" << input_path << output_path;
            p.execute("python", args);
        }
    }

    return image;
}

void compare(){
    QStringList args;
    QProcess p;
    if (flag_2d){
        //args = (QStringList() << config_file <<enrolled_gallery_list_file << enrolled_probe_list_file << score_dir);
        args = (QStringList() << config_file <<enrolled_gallery_list_file << one_probe_list_file << score_dir);
        p.execute("./compare", args);
    }else{
        //args = (QStringList() << "fun_UR3D_compare.py"<< enrolled_gallery_list_file << enrolled_probe_list_file << score_dir);
        args = (QStringList() << "fun_UR3D_compare.py"<< enrolled_gallery_list_file << one_probe_list_file << score_dir);
        p.execute("python", args);
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // remove tool bar
    delete ui->mainToolBar;
    log_file = QDir(log_dir).filePath(QDateTime::currentDateTime().toString("log_yyyy_MM_dd_hh:mm.log"));

    // Add CBL logo
    QPixmap pixmap_logo(":imgs/cbl_logo.png");
    ui->logo_label->setPixmap(pixmap_logo);

    // UR2D pipeline as default
    ui->ur2dRadioButton->setChecked(true);
    ui->ur3dRadioButton->setChecked(false);

    // Gallery Table
    ui->galleryTable->setColumnCount(3);
    ui->galleryTable->setHorizontalHeaderLabels(QStringList()<<"Image"<<"ID"<<"State");
    ui->galleryTable->setColumnWidth(0, thumbnail_size);

    // Probe Table
    //ui->probeTable->setColumnCount(3);
    ui->probeTable->setColumnCount(2);
    //ui->probeTable->setHorizontalHeaderLabels(QStringList() << "Image" <<"ID" << "State");
    ui->probeTable->setHorizontalHeaderLabels(QStringList() << "Image" <<"ID");
    ui->probeTable->setColumnWidth(0, thumbnail_size);

    // create signature & score folders
    utils.createFolders(signature_dir);
    utils.createFolders(score_dir);
    utils.createFolders(log_dir);
    utils.createFolders(QDir(temp_dir).filePath("fimgen"));

    QThreadPool::globalInstance()->setMaxThreadCount(8);
    // create watcher for gallery image scaling
    galleryEnrollWatcher = new QFutureWatcher<QImage>(this);
    galleryEnrollWatcher->setPendingResultsLimit(5);

    connect(galleryEnrollWatcher, SIGNAL(resultReadyAt(int)), SLOT(enrollGalleryReadyAt(int)));
    connect(galleryEnrollWatcher, SIGNAL(finished()), SLOT(enrollGalleryFinished()));
    connect(galleryEnrollWatcher, SIGNAL(progressValueChanged(int)), SLOT(progressBarValuedChanged(int)));

    // create watcher for probe image scaling
    probeEnrollWatcher = new QFutureWatcher<QImage>(this);
    probeEnrollWatcher->setPendingResultsLimit(5);

    connect(probeEnrollWatcher, SIGNAL(resultReadyAt(int)), SLOT(enrollProbeReadyAt(int)));
    connect(probeEnrollWatcher, SIGNAL(finished()), SLOT(enrollProbeFinished()));
    connect(probeEnrollWatcher, SIGNAL(progressValueChanged(int)), SLOT(progressBarValuedChanged(int)));

    connect(ui->probeTable->verticalHeader(), SIGNAL(sectionClicked(int)), SLOT(compareAndShow(int)));
    // compare watcher
    compareWatcher = new QFutureWatcher<void>(this);
    connect(compareWatcher, SIGNAL(finished()), SLOT(compareFinished()));

    // update GUI
    ui->progressBar->setMaximum(100);
    ui->progressBar->setMinimum(0);
    ui->progressBar->setValue(0);

    ui->statusBar->showMessage("Ready.");

    utils.logAppend(log_file, "Initialized. Ready to start URxD.");
}

MainWindow::~MainWindow()
{
    if (galleryEnrollWatcher->isRunning()){
        galleryEnrollWatcher->cancel();
        galleryEnrollWatcher->waitForFinished();
    }
    if (probeEnrollWatcher->isRunning()){
        probeEnrollWatcher->cancel();
        probeEnrollWatcher->waitForFinished();
    }
    if (compareWatcher->isRunning()){
        compareWatcher->cancel();
        compareWatcher->waitForFinished();
    }

    delete galleryEnrollWatcher;
    delete probeEnrollWatcher;
    delete compareWatcher;

    delete ui;
}

void MainWindow::on_addGalleryButton_clicked()
{
    if (galleryEnrollWatcher->isRunning()){
        ui->statusBar->showMessage("Enrolling process is still running. Pls wait.");
        galleryEnrollWatcher->waitForFinished();
    }

    ui->statusBar->showMessage("Open gallery directory");
    QFileDialog dialog;
    gallery_dir = dialog.getExistingDirectory(this, "Open gallery directory", ".", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    qDebug() << "New gallery dir: " << gallery_dir;
    utils.logAppend(log_file, "Add new gallery directory " + gallery_dir);

    // Get the images items
    QDir dir(gallery_dir);
    QFileInfoList file_info_list;
    if (flag_2d){
        // UR2D pipeline
        file_info_list = dir.entryInfoList(support_img2d_ext, QDir::Files|QDir::NoDotAndDotDot);
        ui->statusBar->showMessage("Loading the new 2D gallery images...");
    }else{
        // UR3D pipeline
        file_info_list = dir.entryInfoList(support_img3d_ext, QDir::Files|QDir::NoDotAndDotDot);
        ui->statusBar->showMessage("Loading the new 3D gallery images...");
    }
    int count = 0;
    for(int i = 0; i < file_info_list.size(); i++){
        bool flag = true;
        for(int j = 0; j < gallery_list.size(); j++){
            if (file_info_list.at(i).absoluteFilePath() == gallery_list.at(j))
                flag = false;
        }
        if (flag){
            gallery_list.append(file_info_list.at(i).absoluteFilePath());
            count ++;
        }
    }

    QString msg_str;
    if (count > 0){
        msg_str = "Successful added new gallery. Total image in gallery is "+ QString::number(gallery_list.size())+".";
        ui->galleryTable->setRowCount(gallery_list.size());
        ui->galleryTable->setColumnWidth(0, thumbnail_size);
        // log
        utils.logAppend(log_file, "Total " + QString::number(gallery_list.size()) + " images.");

        // initialization
        ui->progressBar->setMaximum(gallery_list.size());
        enrolled_gallery_list.clear();
        signature_gallery_list.clear();
        // create a thread to update the image
        galleryEnrollWatcher->setFuture(QtConcurrent::mapped(gallery_list, enroll));
    }else{
        msg_str = "Does not find new images to add to gallery. Total image in gallery is "+ QString::number(gallery_list.size())+".";
        ui->statusBar->showMessage(msg_str);
    }
}

void MainWindow::on_addProbeButton_clicked()
{
    if (probeEnrollWatcher->isRunning()){
        ui->statusBar->showMessage("Enrolling process is still running. Pls wait.");
        probeEnrollWatcher->waitForFinished();
    }

    ui->statusBar->showMessage("Open probe directory");
    QFileDialog dialog;
    probe_dir = dialog.getExistingDirectory(this, "Open probe directory", ".", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    qDebug() << "New probe dir: " << probe_dir;
    utils.logAppend(log_file, "Add new probe directory " + probe_dir);

    // Get the images items
    QDir dir(probe_dir);
    QFileInfoList file_info_list;
    if (flag_2d){
        // UR2D pipeline
        file_info_list = dir.entryInfoList(support_img2d_ext, QDir::Files|QDir::NoDotAndDotDot);
        ui->statusBar->showMessage("Loading the new 2D probe images...");
    }else{
        // UR3D pipeline
        file_info_list = dir.entryInfoList(support_img3d_ext, QDir::Files|QDir::NoDotAndDotDot);
        ui->statusBar->showMessage("Loading the new 3D probe images...");
    }
    int count = 0;
    for(int i = 0; i < file_info_list.size(); i++){
        bool flag = true;
        for(int j = 0; j < probe_list.size(); j++){
            if (file_info_list.at(i).absoluteFilePath() == probe_list.at(j))
                flag = false;
        }
        if (flag){
            probe_list.append(file_info_list.at(i).absoluteFilePath());
            count ++;
        }
    }

    QString msg_str;
    if (count > 0){
        msg_str = "Successful added new probe. Total image in probe is "+ QString::number(probe_list.size())+".";
        ui->probeTable->setRowCount(probe_list.size());
        ui->probeTable->setColumnWidth(0, thumbnail_size);
        // log
        utils.logAppend(log_file, "Total " + QString::number(probe_list.size()) + " images.");

        // initialization
        ui->progressBar->setMaximum(probe_list.size());
        enrolled_probe_list.clear();
        signature_probe_list.clear();
        // create a thread to update the image
        probeEnrollWatcher->setFuture(QtConcurrent::mapped(probe_list, enroll));
    }else{
        msg_str = "Does not find new images to add to probe. Total image in probe is "+ QString::number(probe_list.size())+".";
        ui->statusBar->showMessage(msg_str);
    }
}

void MainWindow::on_ur2dRadioButton_clicked()
{
    if (!flag_2d){
        qDebug()<<"select UR2D pipeline";
        gallery_list.clear();
        probe_list.clear();

        enrolled_gallery_list.clear();
        enrolled_probe_list.clear();

        signature_gallery_list.clear();
        signature_probe_list.clear();

        ui->statusBar->showMessage("Select UR2D pipeline. Pls add gallery and probe.");
        ui->probeTable->clear();
        ui->probeTable->setRowCount(0);
        ui->probeTable->setHorizontalHeaderLabels(QStringList() << "Image" <<"ID" << "State");

        ui->galleryTable->clear();
        ui->galleryTable->setRowCount(0);
        ui->galleryTable->setHorizontalHeaderLabels(QStringList() << "Image" <<"ID" << "State");

        ui->progressBar->setValue(0);
        ui->progressBar->update();

        flag_2d = true;
        utils.logAppend(log_file, "/***** Select UR2D pipeline *****/");
    }
}

void MainWindow::on_ur3dRadioButton_clicked()
{
    if (flag_2d){
        qDebug()<<"select UR3D pipeline";
        gallery_list.clear();
        probe_list.clear();
        enrolled_gallery_list.clear();
        enrolled_probe_list.clear();

        signature_gallery_list.clear();
        signature_probe_list.clear();

        ui->statusBar->showMessage("Select UR3D pipeline. Pls add gallery and probe.");
        ui->probeTable->clear();
        ui->probeTable->setRowCount(0);
        ui->probeTable->setHorizontalHeaderLabels(QStringList() << "Image" <<"ID");

        ui->galleryTable->clear();
        ui->galleryTable->setRowCount(0);
        ui->galleryTable->setHorizontalHeaderLabels(QStringList() << "Image" <<"ID" << "State");

        ui->progressBar->setValue(0);
        ui->progressBar->update();

        flag_2d = false;
        utils.logAppend(log_file, "/***** Select UR2D pipeline *****/");
    }
}

void MainWindow::enrollGalleryReadyAt(int i){
    QString input_path = gallery_list.at(i);
    ui->statusBar->showMessage("Enrolling the image: " + input_path);

    QTableWidgetItem *thumbnail = new QTableWidgetItem;
    thumbnail->setData(Qt::DecorationRole, QPixmap::fromImage(galleryEnrollWatcher->resultAt(i)));
    ui->galleryTable->setRowHeight(i, galleryEnrollWatcher->resultAt(i).height());
    ui->galleryTable->setItem(i, 0, thumbnail);
    QString name = QFileInfo(input_path).baseName();
    ui->galleryTable->setItem(i, 1, new QTableWidgetItem(name));

    QString output_path;

    if (flag_2d)
        output_path = QDir(signature_dir).filePath(QFileInfo(input_path).baseName() + ".mat");
    else
        output_path = QDir(signature_dir).filePath(QFileInfo(input_path).baseName() + ".fwv");

    output_path = QFileInfo(output_path).absoluteFilePath();
    if (!QFileInfo(output_path).exists()){
        ui->galleryTable->setItem(i, 2, new QTableWidgetItem("Fail to enroll"));
        utils.logAppend(log_file, "\tFail enroll: " + output_path);
    }else{
        ui->galleryTable->setItem(i, 2, new QTableWidgetItem("Enrollment Successful"));
        utils.logAppend(log_file, "\tSuccess enroll: " + input_path);
        utils.logAppend(log_file, "\tSignature generated to: " + output_path);
    }
}

void MainWindow::enrollGalleryFinished(){
    ui->progressBar->setValue(gallery_list.size());
    ui->progressBar->update();

    // get enroll gallery list
    QString output_path;
    for (int i = 0; i < gallery_list.size(); i++){
        if (flag_2d)
            output_path = QDir(signature_dir).filePath(QFileInfo(gallery_list.at(i)).baseName() + ".mat");
        else
            output_path = QDir(signature_dir).filePath(QFileInfo(gallery_list.at(i)).baseName() + ".fwv");

        if(QFileInfo(output_path).exists()){
            enrolled_gallery_list.push_back(gallery_list.at(i));
            signature_gallery_list.push_back(output_path);
        }
    }

    utils.logAppend(log_file, "Write enrolled gallery list to: " + enrolled_gallery_list_file);
    // generate enrolled gallery list
    QFile file(enrolled_gallery_list_file);
    if (file.open(QIODevice::WriteOnly|QIODevice::Truncate)){
        QTextStream stream(&file);
        for (int i = 0; i < enrolled_gallery_list.size(); i++){
            stream << enrolled_gallery_list.at(i) << "\t" << signature_gallery_list.at(i) << "\t" << QFileInfo(enrolled_gallery_list.at(i)).baseName() << endl;
        }
    }
    file.close();

    QString msg_str = "Successful added new gallery. Total of enrolled gallery is "+ QString::number(enrolled_gallery_list.size())+".";
    ui->statusBar->showMessage(msg_str);

    utils.logAppend(log_file, "Done.");
}

void MainWindow::enrollProbeReadyAt(int i){
    QString input_path = probe_list.at(i);

    ui->statusBar->showMessage("Enrolling the image: " + input_path);

    QTableWidgetItem *thumbnail = new QTableWidgetItem;
    thumbnail->setData(Qt::DecorationRole, QPixmap::fromImage(probeEnrollWatcher->resultAt(i)));
    ui->probeTable->setRowHeight(i, probeEnrollWatcher->resultAt(i).height());
    ui->probeTable->setItem(i, 0, thumbnail);
    QString name = QFileInfo(input_path).baseName();
    ui->probeTable->setItem(i, 1, new QTableWidgetItem(name));

    QString output_path;

    if (flag_2d)
        output_path = QDir(signature_dir).filePath(QFileInfo(input_path).baseName() + ".mat");
    else
        output_path = QDir(signature_dir).filePath(QFileInfo(input_path).baseName() + ".fwv");

    output_path = QFileInfo(output_path).absoluteFilePath();
    if (!QFileInfo(output_path).exists()){
        //ui->probeTable->setItem(i, 2, new QTableWidgetItem("Fail to enroll"));
        utils.logAppend(log_file, "\tFail enroll: " + output_path);
    }else{
        //ui->probeTable->setItem(i, 2, new QTableWidgetItem("Enrollment Successful"));
        utils.logAppend(log_file, "\tSuccess enroll: " + input_path);
        utils.logAppend(log_file, "\tSignature generated to: " + output_path);
    }

    //ui->progressBar->setValue(i);
    //ui->progressBar->update();
}

void MainWindow::enrollProbeFinished(){
    ui->progressBar->setValue(probe_list.size());
    ui->progressBar->update();

    // get enroll probe list
    QString output_path;
    for (int i = 0; i < probe_list.size(); i++){
        if (flag_2d)
            output_path = QDir(signature_dir).filePath(QFileInfo(probe_list.at(i)).baseName() + ".mat");
        else
            output_path = QDir(signature_dir).filePath(QFileInfo(probe_list.at(i)).baseName() + ".fwv");

        if(QFileInfo(output_path).exists()){
            enrolled_probe_list.push_back(probe_list.at(i));
            signature_probe_list.push_back(output_path);
        }
    }

    utils.logAppend(log_file, "Write enrolled probe list to: " + enrolled_probe_list_file);
    // generate enrolled probe list
    QFile file(enrolled_probe_list_file);
    if (file.open(QIODevice::WriteOnly|QIODevice::Truncate)){
        QTextStream stream(&file);
        for (int i = 0; i < enrolled_probe_list.size(); i++){
            stream << enrolled_probe_list.at(i) << "\t" << signature_probe_list.at(i) << "\t" << QFileInfo(enrolled_probe_list.at(i)).baseName() << endl;
        }
    }
    file.close();

    QString msg_str = "Successful added new probe. Total of enrolled probe is "+ QString::number(enrolled_probe_list.size())+".";
    ui->statusBar->showMessage(msg_str);

    utils.logAppend(log_file, "Done.");
}

void MainWindow::compareAndShow(int num){
    utils.logAppend(log_file, "Select " + QString::number(num) + "th row.");

    QString input_path = probe_list.at(num);
    current_selected_id = QFileInfo(input_path).baseName();
    QString signature_path;
    if (flag_2d)
        signature_path = QDir(signature_dir).filePath(QFileInfo(input_path).baseName() + ".mat");
    else
        signature_path = QDir(signature_dir).filePath(QFileInfo(input_path).baseName() + ".fwv");

    if (!QFileInfo(signature_path).exists()){
        ui->statusBar->showMessage("Fail to generatue features");
        utils.logAppend(log_file, "Cannot find the feature");
        return;
    }else{
        QString score_file = QDir(score_dir).filePath(current_selected_id+".txt");
        if (!QFileInfo(score_file).exists()){
            QFile tmp_file(one_probe_list_file);
            if (tmp_file.open(QIODevice::WriteOnly)){
                QTextStream stream(&tmp_file);
                stream << input_path << "\t" << signature_path << "\t" << QFileInfo(input_path).baseName() << endl;
            }
            tmp_file.close();

            ui->galleryTable->clear();
            ui->galleryTable->setHorizontalHeaderLabels(QStringList()<<"Image"<<"ID"<<"Score");
            ui->galleryTable->setRowCount(0);

            if (enrolled_gallery_list.size()>0){
                ui->statusBar->showMessage("Comparing...Pls wait...");
                ui->progressBar->setValue(0);
                ui->progressBar->update();

                if (compareWatcher->isRunning())
                    compareWatcher->waitForFinished();

                compareWatcher->setFuture(QtConcurrent::run(compare));
            }else{
                ui->statusBar->showMessage("Pls add gallery.");
            }
        }else{
            try{
                if (QFileInfo(score_file).exists()){
                    utils.logAppend(log_file, "\tFound score file: " + score_file);
                    // update gallery table
                    QFile file(score_file);
                    if(file.open(QIODevice::ReadOnly)){
                        QTextStream stream(&file);
                        qDebug()<<"update gallery table";
                        QVector<QStringList> lines;
                        while (!stream.atEnd()) {
                            QString line = stream.readLine();
                            QStringList list = line.split("\t");
                            lines.push_back(list);
                        }

                        int MIN = qMin(5, lines.size());
                        ui->galleryTable->setRowCount(MIN);
                        for (int i = 0; i < MIN; i++){

                            QStringList list = lines.at(i);

                            QString image_path = list.at(1);
                            if(!flag_2d){
                                QFileInfo fileInfo(list.at(1));
                                QString ext = fileInfo.suffix();
                                QStringList img2D_ext_list = (QStringList() << "bmp" << "jpg");
                                for (int j = 0; j < img2D_ext_list.size(); j++){
                                    QString new_ext = img2D_ext_list.at(j);
                                    image_path = image_path.replace(ext, new_ext);
                                    if(QFileInfo(image_path).exists())
                                        break;
                                }
                            }

                            QTableWidgetItem *thumbnail = new QTableWidgetItem;
                            QImage img = scale(image_path);
                            thumbnail->setData(Qt::DecorationRole, QPixmap::fromImage(img));
                            ui->galleryTable->setRowHeight(i, img.height());
                            ui->galleryTable->setItem(i, 0, thumbnail);
                            // name
                            ui->galleryTable->setItem(i, 1, new QTableWidgetItem(list.at(2)));
                            // score
                            double number = list.at(0).toDouble();
                            if (number > 1)
                                number = 1;
                            QString score_num;
                            score_num = score_num.sprintf("%.2f", number);
                            ui->galleryTable->setItem(i, 2, new QTableWidgetItem(score_num));
                        }
                    }
                    ui->statusBar->showMessage("Loaded the comparison result.");
                }else{
                    utils.logAppend(log_file, "\tDoes not found score file: " + score_file);
                    ui->statusBar->showMessage("Cannot find the comparison result. Pls click compare.");
                }
            }catch(std::exception &e){
                qDebug()<<"Error in reading score file: " << score_file;
                utils.logAppend(log_file, "\tError in reading score file: " + score_file);
            }
        }
    }

}


void MainWindow::compareFinished(){
    ui->statusBar->showMessage("Comparison done.");
    ui->progressBar->setMaximum(1);
    ui->progressBar->setValue(1);
    ui->progressBar->update();

    QString score_file = QDir(score_dir).filePath(current_selected_id+".txt");
    try{
        if (QFileInfo(score_file).exists()){
            utils.logAppend(log_file, "\tFound score file: " + score_file);
            // update gallery table
            QFile file(score_file);
            if(file.open(QIODevice::ReadOnly)){
                QTextStream stream(&file);
                qDebug()<<"update gallery table";
                QVector<QStringList> lines;
                while (!stream.atEnd()) {
                    QString line = stream.readLine();
                    QStringList list = line.split("\t");
                    lines.push_back(list);
                }

                int MIN = qMin(5, lines.size());
                ui->galleryTable->setRowCount(MIN);
                for (int i = 0; i < MIN; i++){

                    QStringList list = lines.at(i);

                    QString image_path = list.at(1);
                    if(!flag_2d){
                        QFileInfo fileInfo(list.at(1));
                        QString ext = fileInfo.suffix();
                        QStringList img2D_ext_list = (QStringList() << "bmp" << "jpg");
                        for (int j = 0; j < img2D_ext_list.size(); j++){
                            QString new_ext = img2D_ext_list.at(j);
                            image_path = image_path.replace(ext, new_ext);
                            if(QFileInfo(image_path).exists())
                                break;
                        }
                    }

                    QTableWidgetItem *thumbnail = new QTableWidgetItem;
                    QImage img = scale(image_path);
                    thumbnail->setData(Qt::DecorationRole, QPixmap::fromImage(img));
                    ui->galleryTable->setRowHeight(i, img.height());
                    ui->galleryTable->setItem(i, 0, thumbnail);
                    // name
                    ui->galleryTable->setItem(i, 1, new QTableWidgetItem(list.at(2)));
                    // score
                    double number = list.at(0).toDouble();
                    if (number > 1)
                        number = 1;
                    QString score_num;
                    score_num = score_num.sprintf("%.2f", number);
                    ui->galleryTable->setItem(i, 2, new QTableWidgetItem(score_num));
                }
            }
            ui->statusBar->showMessage("Loaded the comparison result.");
        }else{
            utils.logAppend(log_file, "\tDoes not found score file: " + score_file);
            ui->statusBar->showMessage("Cannot find the comparison result. Pls click compare.");
        }
    }catch(std::exception &e){
        qDebug()<<"Error in reading score file: " << score_file;
        utils.logAppend(log_file, "\tError in reading score file: " + score_file);
    }
}

void MainWindow::progressBarValuedChanged(int progressValue){
    ui->progressBar->setValue(progressValue);
    ui->progressBar->update();
}

void MainWindow::on_probeTable_cellDoubleClicked(int row, int column)
{
    qDebug() << "double clicked on probe table";
    if((!flag_2d) && (column == 0)){
        QString input_path = probe_list.at(row);
        current_selected_path = input_path;
    }
}
