#ifndef UTILS_H
#define UTILS_H
#include <QMap>
#include <QtXml>
#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QDebug>

class Utils
{
public:
    Utils();
    bool createFolders(const QString folderpath);
    void get2Dimage4wrl(const QStringList fileList, QStringList &image2dList);

    QStringList img2dExtList = (QStringList()<<"bmp" <<"jpg");

    void logAppend(const QString log_file, const QString log_str);
};

#endif // UTILS_H
