#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "utils.h"
#include <QFileInfoList>
#include <QtConcurrent/QtConcurrent>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    //void on_compareButton_clicked();

    void on_addGalleryButton_clicked();

    void on_addProbeButton_clicked();

    void on_ur2dRadioButton_clicked();

    void on_ur3dRadioButton_clicked();

    void enrollGalleryReadyAt(int i);

    void enrollGalleryFinished();

    void enrollProbeReadyAt(int i);

    void enrollProbeFinished();

    void compareFinished();

    void compareAndShow(int);

    void progressBarValuedChanged(int);

    void on_probeTable_cellDoubleClicked(int row, int column);

private:
    Ui::MainWindow *ui;
    QFutureWatcher<QImage> *galleryEnrollWatcher;
    QFutureWatcher<QImage> *probeEnrollWatcher;

    QFutureWatcher<void> *compareWatcher;

    QStringList gallery_list;
    QStringList probe_list;

    QStringList enrolled_gallery_list;
    QStringList enrolled_probe_list;

    QStringList signature_gallery_list;
    QStringList signature_probe_list;

    QStringList support_img2d_ext = (QStringList() << "*.bmp");
    QStringList support_img3d_ext = (QStringList() << "*.wrl");

    Utils utils;

//public:
//    void enroll_process(int);

};

#endif // MAINWINDOW_H
