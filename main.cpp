#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    // set icon
    // QIcon icon(":imgs/icon.png");
    // w.setWindowIcon(icon);
    w.show();

    return a.exec();
}
