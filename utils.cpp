#include "utils.h"

Utils::Utils()
{

}

bool Utils::createFolders(const QString folderpath){
    QDir dir(folderpath);
    if(!dir.exists()){
        dir.mkpath(dir.absolutePath());
    }
    return true;
}

void Utils::get2Dimage4wrl(const QStringList fileList, QStringList &image2dList){
    for(int i = 0; i < fileList.size(); i++){
        QString filePath = fileList.at(i);
        QFileInfo fileInfo(filePath);
        QString ext = fileInfo.suffix();
        QString img2dFilePath;
        for (int j = 0; j < img2dExtList.size(); j++){
            img2dFilePath = filePath.replace(ext, img2dExtList.at(j));
            if(QFileInfo(img2dFilePath).exists())
                break;
        }
        image2dList.push_back(img2dFilePath);
    }
}

void Utils::logAppend(const QString log_file, const QString log_str){
    QFile file(log_file);
    if (file.open(QIODevice::Append|QIODevice::Text)){
        QTextStream stream(&file);
        stream << log_str << endl;
    }
    file.close();
}
