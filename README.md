# This is the URxD GUI for running the URxD pipeline.
The repository is created on Oct. 20, 2016
This GUI is designed by [Xiang Xu](shownx.github.io)
The last changed is made by Xiang Xu on Oct. 22, 2016.

# How to use


# Known Bugs:
* The window will frozen when load too many very large pictures in gallery and probe.
* Solution: 1. show maxmum 200 images 2. Add threads to run process (weird.)

# TODO:
* Add UR2D, UR3D pipeline.
* Add render 3D dialog when double clicking the table in ur3d mode.
