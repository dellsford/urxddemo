#-------------------------------------------------
#
# Project created by QtCreator 2016-10-20T22:23:17
#
#-------------------------------------------------

QT       += core gui concurrent xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = URxDDemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    utils.cpp

HEADERS  += mainwindow.h \
    utils.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc

DISTFILES += \
    README.md
